"vim: set foldmethod=marker
" needed for dein
set runtimepath+=~/Repos/dein.vim

" map leaders
let maplocalleader = ","

" plugins {{{
if dein#load_state(expand('~/.local/share/dein'))
	call dein#begin(expand('~/.local/share/dein'))
	call dein#add('~/Repos/dein.vim')
	"!sort -k2 -t/
	call dein#add('Konfekt/FastFold')
	call dein#add('junegunn/fzf', { 'build': './install', 'merged': 0 }) 
	call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })
	call dein#add('zivyangll/git-blame.vim')
	call dein#add('mboughaba/i3config.vim')
	call dein#add('xPMo/neovim-colors-solarized-truecolor', { 'rev': 'fork'})
	call dein#add('vim-scripts/OmniCppComplete')
	call dein#add('rust-lang/rust.vim')
	call dein#add('tmhedberg/SimpylFold')
	call dein#add('ervandew/supertab')
	call dein#add('vim-syntastic/syntastic')
	call dein#add('jreybert/vimagit')
	call dein#add('altercation/vim-colors-solarized')
	call dein#add('airblade/vim-gitgutter')
	call dein#add('jamessan/vim-gnupg')
	call dein#add('bitc/vim-hdevtools')
	call dein#add('michaeljsmith/vim-indent-object')
	call dein#add('vim-pandoc/vim-pandoc')
	call dein#add('vim-pandoc/vim-pandoc-syntax')
	call dein#add('racer-rust/vim-racer')
	call dein#add('tpope/vim-surround')
	call dein#add('lervag/vimtex')
	call dein#end()
	call dein#save_state()
endif
" }}}
"
"
set exrc            " Per-dir .vimrc files
set history=200     " keep 200 lines of command line history
set ruler           " show the cursor position all the time
set showcmd         " display incomplete commands
set wildmenu        " display completion matches in a status line
set wildmode=longest,list,full       " completion mode
set omnifunc=syntaxcomplete#Complete " ^
set spell           " use spellchecking

set ttimeout        " time out for key codes
set ttimeoutlen=20  " wait up to 20ms after Esc for special key

" Show lines of context around the cursor at the top/bottom
set scrolloff=2

" change working directory to file location
set autochdir

" buffers can be switched from without saving
set hidden

" indent {{{
set autoindent
" Visual-mode indenting with Tab/Shift-Tab
vmap <Tab> >gv
vmap <S-Tab> <gv
inoremap <S-Tab> <C-D>
set shiftwidth=4
set tabstop=4
set softtabstop=4
filetype plugin indent on
" }}}

" make Y like C and D
nnoremap Y y$

" cursor keys scroll visual lines (word wrap) {{{
imap <up> <C-O>gk
imap <down> <C-O>gj
nmap <up> gk
nmap <down> gj
vmap <up> gk
vmap <down> gj
" }}}

" mouse
set mouse="a"

" search {{{
" Incremental searching
set ignorecase
set smartcase
" Double escape to clear search highlight
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>

if executable("rg")
	set grepprg=rg\ --vimgrep\ --no-heading
	set grepformat=%f:%l:%c:%m,%f:%l:%m
	command! -nargs=+ RG execute 'silent grep! <args>' | copen 15
else
	set grepprg=grep\ -nH\ $*
endif
" }}}


augroup vimStartup
	autocmd!
	" When editing a file, always jump to the last known cursor position.
	" Don't do it when the position is invalid, when inside an event handler
	" (happens when dropping a file on gvim) and for a commit message (it's
	" likely a different one than last time).
	autocmd BufReadPost *
	  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
	  \ |   exe "normal! g`\""
	  \ | endif
augroup END

" color {{{
set list
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·
augroup vimrc
	autocmd!
	set background=dark
	let g:solarized_termtrans=1
	colorscheme solarized
	" assume 256color, urxvt's terminfo is broken
	" solarized sets Normal fg text to blue for some reason,
	" fix that and set other highlight classes I want
	" TODO: I might fork solarized.vim to assume 256 and italics
	" and set these colors instead
	autocmd ColorScheme * highlight NonText ctermfg=0 cterm=underline gui=underline guifg=#073642
			\ | highlight SpecialKey ctermbg=0
			\ | highlight Comment ctermfg=242 cterm=italic
			\ | highlight Normal ctermfg=7
			\ | highlight Identifier ctermfg=NONE
			\ | highlight Function ctermfg=12
			\ | highlight Character ctermfg=33
			\ | highlight Number ctermfg=4 cterm=bold
			\ | highlight Boolean cterm=bold
			\ | highlight Search ctermbg=none ctermfg=none
	call dein#add('vim-syntastic/syntastic')
augroup END
" }}}

" diff current buffer with the file it was loaded from
command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
	  \ | wincmd p | diffthis

command -nargs=* G !git <args>

" filetype specific {{{
" load vimtex
let g:tex_flavor='latex'
" enable folding
let g:vimtex_fold_enabled=1
" only fold sections
let g:vimtex_fold_types = {
           \ 'envs' : {
           \   'whitelist' : ['frame'],
           \ },
\}
" integrate zathura
let g:vimtex_view_method = 'zathura'
" disable 'unwanted space' messages
let g:syntastic_quiet_messages = { "regex": [
        \ '\mpossible unwanted space at "{"',
        \ ] }
" haskell hdevtools integration
autocmd FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
autocmd FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsClear<CR>
" spaces recommended in Haskell
autocmd FileType haskell set expandtab
" git commit line length at 72, wrap on word
autocmd FileType gitcommit set tw=72 lbr

" read-only non-text filetypes
autocmd BufReadPre *.doc silent set ro
autocmd BufReadPost *.doc silent %!pandoc "%" -t markdown -o -

autocmd BufReadPre *.odt,*.odp silent set ro
autocmd BufReadPost *.odt,*.odp silent %!pandoc "%" -t markdown -o -

autocmd BufReadPre *.pdf silent set ro
autocmd BufReadPost *.pdf silent %!pdftotext -nopgbrk -layout -q -eol unix "%" - | fmt -w78

autocmd BufReadPre *.rtf silent set ro
autocmd BufReadPost *.rtf silent %!pandoc "%" -t markdown -o -

autocmd BufNewFile,BufRead *.zsh-theme setlocal filetype=zsh

" autoformat json
autocmd FileType json silent %!jq . %

" fold manpages
autocmd FileType man set foldmethod=indent shiftwidth=3 foldnestmax=2
" }}}

" fzf.vim {{{
nnoremap ZB :Buffers<cr>
nnoremap ZF :Files<cr>
nnoremap ZL :Lines<cr>
" neovim-only
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
let g:fzf_layout = { 'window': '10split enew' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
" }}}
" vim: set foldmethod=marker:
